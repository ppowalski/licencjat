import { NgModule } from '@angular/core';

import { MatButtonModule } from "@angular/material";
import { MatInputModule } from "@angular/material";
import { MatFormFieldModule } from "@angular/material";
import { MatIconModule } from "@angular/material";
import { MatProgressSpinnerModule } from "@angular/material";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatListModule } from "@angular/material";
import { MatProgressBarModule } from "@angular/material/progress-bar";
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatCheckboxModule } from '@angular/material/checkbox';


const components = [
  MatButtonModule,
  MatInputModule,
  MatIconModule,
  MatFormFieldModule,
  MatProgressSpinnerModule,
  MatListModule,
  MatSidenavModule,
  MatProgressBarModule,
  MatTableModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatCheckboxModule,
];

@NgModule({
  imports: [components],
  exports: [components]
})
export class MaterialModule { }
