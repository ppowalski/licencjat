import { Injectable } from '@angular/core';
import { Router, CanActivate } from "@angular/router";
import { AuthService } from "./auth.service";

@Injectable({
  providedIn: 'root'
})
export class GuardService implements CanActivate {
  constructor(private client: AuthService, private router: Router) { }

  canActivate(): boolean {
    if(!this.client.isAuthenticated()) {
      this.router.navigate(['login']);
      return false;
    } else {
      return true;
    }
  }
}
