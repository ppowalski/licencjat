import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate} from '@angular/router';
import { GuardService } from "./guard.service";
import { LoginComponent } from "./login/login.component";
import { HomeComponent } from "./home/home.component";
import { TreningsComponent } from "./trenings/trenings.component";
import { MealsComponent } from "./meals/meals.component";
import { ProductsComponent } from "./products/products.component";
import { RegisterComponent } from "./register/register.component";
import { ForgotPasswordComponent } from "./forgot-password/forgot-password.component";

const routes: Routes = [
    {
        path: 'login',
        component: LoginComponent,
    },
    {
        path: 'register',
        component: RegisterComponent,
    },
    {
        path: 'forgot-password',
        component: ForgotPasswordComponent,
    },
    {
        path: '',
        component: HomeComponent,
        canActivate: [GuardService],
    },
    {
        path: 'trenings',
        component: TreningsComponent,
        canActivate: [GuardService],
    },
    {
        path: 'meals',
        component: MealsComponent,
        canActivate: [GuardService],
    },
    {
        path: 'products',
        component: ProductsComponent,
        canActivate: [GuardService],
    },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
