import { Component, OnInit } from '@angular/core';
import { AuthService } from "../auth.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-trenings',
  templateUrl: './trenings.component.html',
  styleUrls: ['./trenings.component.css']
})
export class TreningsComponent implements OnInit {

  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit() {
  }

  logout()
  {
    this.auth.logout();
    this.router.navigate(['login']);
  }
}
