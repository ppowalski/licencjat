import { Component, OnInit } from '@angular/core';
import { AuthService } from "../auth.service";
import { Router } from "@angular/router";

export interface ProductsElements {
    name: string;
    kcal: number;
    fat: number;
    saturatedFat: number;
    protein: number;
    sugar: number;
    salt: number;
    carbohydrates: number;
}

const DATA: ProductsElements[] = [
    {name: 'Chleb', kcal: 238, protein: 6.16, fat: 1.2, carbohydrates: 50, salt: 0, sugar: 0, saturatedFat: 0},
    {name: 'Chleb', kcal: 238, protein: 6.16, fat: 1.2, carbohydrates: 50, salt: 0, sugar: 0, saturatedFat: 0},
    {name: 'Chleb', kcal: 238, protein: 6.16, fat: 1.2, carbohydrates: 50, salt: 0, sugar: 0, saturatedFat: 0},
    {name: 'Chleb', kcal: 238, protein: 6.16, fat: 1.2, carbohydrates: 50, salt: 0, sugar: 0, saturatedFat: 0},
    {name: 'Chleb', kcal: 238, protein: 6.16, fat: 1.2, carbohydrates: 50, salt: 0, sugar: 0, saturatedFat: 0},
    {name: 'Chleb', kcal: 238, protein: 6.16, fat: 1.2, carbohydrates: 50, salt: 0, sugar: 0, saturatedFat: 0},
    {name: 'Chleb', kcal: 238, protein: 6.16, fat: 1.2, carbohydrates: 50, salt: 0, sugar: 0, saturatedFat: 0},
    {name: 'Chleb', kcal: 238, protein: 6.16, fat: 1.2, carbohydrates: 50, salt: 0, sugar: 0, saturatedFat: 0},
    {name: 'Chleb', kcal: 238, protein: 6.16, fat: 1.2, carbohydrates: 50, salt: 0, sugar: 0, saturatedFat: 0},
    {name: 'Chleb', kcal: 238, protein: 6.16, fat: 1.2, carbohydrates: 50, salt: 0, sugar: 0, saturatedFat: 0},
    {name: 'Chleb', kcal: 238, protein: 6.16, fat: 1.2, carbohydrates: 50, salt: 0, sugar: 0, saturatedFat: 0},
    {name: 'Chleb', kcal: 238, protein: 6.16, fat: 1.2, carbohydrates: 50, salt: 0, sugar: 0, saturatedFat: 0},
    {name: 'Chleb', kcal: 238, protein: 6.16, fat: 1.2, carbohydrates: 50, salt: 0, sugar: 0, saturatedFat: 0},
];

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  dataSource = DATA;
  displayedColumns: string[] = ['name', 'kcal', 'carbohydrates', 'protein', 'fat', 'actions'];

  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit() {
  }

  logout()
  {
    this.auth.logout();
    this.router.navigate(['login']);
  }

}
