import { Component, OnInit } from '@angular/core';
import { AuthService } from "../auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username: string = '';
  password: string = '';

  show: boolean = true;
  loading: boolean = false;
  error: string = '';

  constructor(private client: AuthService) { }

  ngOnInit() {
  }

  formSubmit() {
      if (this.username.length > 0 && this.password.length > 0) {
          this.loading = true;
          this.show = false;
          this.client.login(this);
      }
  }
}
