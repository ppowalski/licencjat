import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import * as jwt_decode from "jwt-decode";
import {LoginComponent} from "./login/login.component";
import {RegisterComponent} from "./register/register.component";
import {FormControl} from "@angular/forms";

const AUTH = 'http://localhost:8000/api/v1/auth';
const REGISTER = 'http://localhost:8000/api/v1/register';

const HTTP_HEADERS = new HttpHeaders()
    .set('Accept', 'application/json')
    .set('Content-Type', 'application/json');

@Injectable({
    providedIn: 'root'
})
export class AuthService {
  loading: boolean = false;

  constructor(private httpClient: HttpClient, private router: Router) { }

 callbackLogin(result, object: LoginComponent) {
      if(typeof result['token'] != "undefined") {
        localStorage.setItem('authToken', result['token']);
        object.error = '';
        this.router.navigate(['']);
      } else if(typeof result['message'] != "undefined") {
        object.error = result['message'];
      }

     object.loading = false;
     object.show = true;
 }

  callbackRegister(result, object: RegisterComponent) {
    if(typeof result['success'] != "undefined" && result['success']) {
      localStorage.setItem('authToken', result['token']);
      object.error = '';
      this.router.navigate(['login']);
    } else if(typeof result['message'] != "undefined") {
      object.error = result['message'];
    }

    object.loading = false;
    object.show = true;
  }

 login(object: LoginComponent) {
    this.loading = true;
    let body = {
      'username': object.username,
      'password': object.password,
    };

    let response = this.httpClient.post(AUTH, body, {headers: HTTP_HEADERS}).subscribe(
        (value) => {
            this.callbackLogin(value, object);
        }, (error) => {
            this.callbackLogin(error, object);
        });
  }

  register(object: RegisterComponent) {
    this.loading = true;
    let body = {
      'username': object.username,
      'password': object.password,
      'passwordRepeat': object.passwordRepeat,
      'email': object.email,
      'terms': object.terms,
    };

    let response = this.httpClient.post(REGISTER, body, {headers: HTTP_HEADERS}).subscribe(
      (value) => {
        this.callbackRegister(value, object);
      }, (error) => {
        this.callbackRegister(error, object);
      });
  }

  logout() {
    localStorage.removeItem('authToken');
  }

  isAuthenticated() {
    return localStorage.getItem('authToken') !== null;
  }
}
