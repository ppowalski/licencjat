<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('usr_id');
            $table->string('username', 255);
            $table->string('email', 255);
            $table->string('password');
            $table->string('photo', 255)->nullable();
            $table->string('weight', 255)->nullable();
            $table->string('height', 255)->nullable();
            $table->date('birthday')->nullable();
            $table->boolean('isActive')->default(false);
            $table->string('token');
            $table->boolean('deleted')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
