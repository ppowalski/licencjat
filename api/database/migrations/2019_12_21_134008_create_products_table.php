<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('prod_id');
            $table->string('prod_name', 255);
            $table->string('prod_photo', 255)->nullable();

            // per 100g
            $table->double('prod_kcal')->nullable();
            $table->double('prod_fat')->nullable();
            $table->double('prod_protein')->nullable();
            $table->double('prod_saturated_fat')->nullable();
            $table->double('prod_sugar')->nullable();
            $table->double('prod_salt')->nullable();
            $table->double('prod_carbohydrates')->nullable();

            $table->boolean('prod_deleted')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
