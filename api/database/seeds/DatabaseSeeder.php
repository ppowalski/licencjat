<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username' => 'Patryk',
            'email' => 'kontakt@patrykpowalski.pl',
            'password' => bcrypt('zaq12WSX'),
        ]);
    }
}
