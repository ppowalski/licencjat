<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::prefix('v1')->group(function () {
    // Auth
    Route::post('/register', ['as'=> 'register', 'uses' => 'AuthController@register']);
    Route::post('/auth', ['as' => 'login', 'uses'=> 'AuthController@login']);
    Route::get('/logout', 'AuthController@logout');
    Route::get('/confirm-account', ['as' => 'confirmAccount', 'uses'=> 'AuthController@confirmAccount']);

    // Users
    Route::get('/users', 'UsersController@index');
    Route::get('/users/{id}', 'UsersController@element');
    Route::put('/users/{id}', 'UsersController@update');
    Route::post('/users', 'UsersController@create');
    Route::delete('/users/{id}', 'UsersController@destroy');

    // Products
    Route::get('/products', 'ProductsController@index');
    Route::get('/products/{id}', 'ProductsController@element');
    Route::put('/products/{id}', 'ProductsController@update');
    Route::post('/products', 'ProductsController@create');
    Route::delete('/products/{id}', 'ProductsController@destroy');
});
