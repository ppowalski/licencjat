<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    protected $table = 'users';
    protected $primaryKey = 'usr_id';

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'token', 'isActive',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'token',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [
            'id' => $this->usr_id,
            'username' => $this->username,
            'photo' => $this->photo,
        ];
    }

    public function getUserById($usr_id)
    {
        $result = DB::table($this->table)
                ->where('usr_id', $usr_id)
                ->where('deleted', false)
                ->get();

        return $result;
    }

    public function getUserForRegistration($username, $email)
    {
        return DB::table($this->table)
            ->where(function ($query) use ($username, $email) {
                $query->where('email', $email)
                    ->orWhere('username', $username);
            })
            ->where('deleted', false)
            ->first();
    }

    public function getUserByToken($token)
    {
        return DB::table($this->table)
            ->where('token', $token)
            ->where('isActive', false)
            ->where('deleted', false)
            ->first();
    }


}
