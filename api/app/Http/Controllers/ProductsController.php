<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        return response()->json([
            'test' => 'index',
        ]);
    }

    public function element($id)
    {
        return response()->json([
            'test' => 'element',
            'id' => $id,
        ]);
    }

    public function update($id)
    {
        return response()->json([
            'test' => 'update',
            'id' => $id,
        ]);
    }

    public function create()
    {
        return response()->json([
            'test' => 'create',
        ]);
    }

    public function destroy($id)
    {
        return response()->json([
            'test' => 'destroy',
            'id' => $id,
        ]);
    }
}
