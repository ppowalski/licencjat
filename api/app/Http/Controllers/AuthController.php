<?php

namespace App\Http\Controllers;

use App\Mail\ConfirmAccount;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except(['login', 'register', 'confirmAccount']);
    }

    public function login(Request $request)
    {
        $data = [
            'username' => !empty($request->username) ? $request->username : '',
            'password' => !empty($request->password) ? $request->password : '',
        ];

        if(!$token = auth()->attempt($data)) {
            return response()->json([
                'success' => false,
                'message' => 'Nieprawidłowa nazwa użytkownika lub hasło.',
            ], 200);
        }

        if(empty(auth()->user()['isActive']) || !auth()->user()['isActive']) {
            return response()->json([
                'success' => false,
                'message' => 'Konto nie zostało zweryfikowane. Możesz zweryfikować konto poprzez adres email.',
            ], 200);
        }

        return response()->json([
            'success' => true,
            'token' => $token,
            'token_type' => 'bearer',
            'expire_date' => auth()->factory()->getTTL() * 60,
        ], 200);
    }

    public function logout()
    {
        auth()->logout();

        return response()->json([
            'success' => true,
            'message' => 'wylogowano',
        ], 200);
    }

    public function confirmAccount(Request $request)
    {
        if(!empty($request->token)) {
            $modelUsers = new User();
            $user = $modelUsers->getUserByToken($request->token);

            if(empty($user)) {
                return response()->json([
                    'success' => false,
                    'message' => 'Wprowadzono nieprawidłowo token.'
                ], 200);
            }

            User::find($user->usr_id)->update(['isActive' => true]);

            return response()->json([
                'success' => true,
                'message' => 'Aktywowano konto.'
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Wprowadzono nieprawidłowy token.'
            ], 200);
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|min:3|max:32',
            'email' => 'required|email',
            'password' => 'required|min:8',
            'passwordRepeat' => 'required|same:password',
            'terms' => 'required',
        ]);

        if($validator->fails()) {
            if(!empty($validator->errors()->first('password'))) {
                return response()->json([
                    'success' => false,
                    'message' => 'Podane hasło jest nieprawidłowe, musi zawierać przynajmniej 8 znaków.'
                ], 200);
            } else if(!empty($validator->errors()->first('username'))) {
                return response()->json([
                    'success' => false,
                    'message' => 'Nazwa użytkownika powinna zawierać od 3 do 32 znaków.'
                ], 200);
            } else if(!empty($validator->errors()->first('passwordRepeat'))) {
                return response()->json([
                    'success' => false,
                    'message' => 'Podane hasła nie są identyczne.'
                ], 200);
            } else if(!empty($validator->errors()->first('email'))) {
                return response()->json([
                    'success' => false,
                    'message' => 'Podany email jest nieprawidłowy.'
                ], 200);
            } else if(!empty($validator->errors()->first('terms'))) {
                return response()->json([
                    'success' => false,
                    'message' => 'Nie zaakceptowano regulaminu.'
                ], 200);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Wystąpił bład.'
                ], 200);
            }
        }

        $modelUsers = new User();
        $user = $modelUsers->getUserForRegistration($request->username, $request->email);

        if(!empty($user)) {
            return response()->json([
                'success' => false,
                'message' => 'Istnieje już użytkownik o takiej nazwie użytkownika lub adresie email',
                'fields' => ['username'],
            ], 200);
        }

        // Validation success

        $data['username'] = $request->username;
        $data['email'] = $request->email;
        $data['password'] = Hash::make($request->password);
        $data['token'] = Hash::make(Str::random(60) . $request->password . $request->username);

        User::create($data);

        Mail::to($data['email'])->send(new ConfirmAccount($data['username'], $data['token']));

        return response()->json([
            'success' => true,
            'message' => 'Utworzono konto',
        ], 200);
    }
}
