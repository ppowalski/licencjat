<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        return response()->json([
            'test' => 'index',
        ]);
    }

    public function element($id)
    {
        $modelUsers = new User;
        $user = $modelUsers->getUserById($id);

        return response()->json([
            'user' => $user,
        ]);
    }

    public function update($id)
    {
        return response()->json([
            'test' => 'update',
            'id' => $id,
        ]);
    }

    public function create(Request $request)
    {
        $modelUsers = new User;

        $modelUsers->usr_name = $request->username;

        return response()->json([
            'test' => $modelUsers,
        ]);
    }

    public function destroy($id)
    {
        return response()->json([
            'test' => 'destroy',
            'id' => $id,
        ]);
    }
}
