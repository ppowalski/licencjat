<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    const CREATED_AT = 'prod_creation_time';
    const UPDATED_AT = 'prod_last_update';
    
    protected $table = 'products';
    protected $primaryKey = 'prod_id';
}
